﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace PushTestOnKilledApp.Droid
{
    [Activity(Label = "PushTestOnKilledApp", 
        Icon = "@drawable/icon", 
        Theme = "@style/MainTheme", 
        MainLauncher = false, 
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, 
        LaunchMode = LaunchMode.SingleTop)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
        }

        protected override void OnNewIntent(Intent intent)
        {
            if ((Intent.Flags & ActivityFlags.LaunchedFromHistory) == ActivityFlags.LaunchedFromHistory)
                return;

            base.OnNewIntent(intent);
        }
    }
}

