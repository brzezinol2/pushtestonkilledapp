﻿
using Android.App;
using Android.OS;
using Microsoft.AppCenter.Push;

namespace PushTestOnKilledApp.Droid
{
    [Activity(Label = "PushTestOnKilledApp", 
        Icon = "@drawable/icon",
        Theme = "@style/MyTheme.Splash", 
        MainLauncher = true, 
        NoHistory = true)]
    public class SplashActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        readonly static string TAG = typeof(SplashActivity).Name;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            Push.SetSenderId("805986297631");

            LoadApplication(new App());
        }
    }
}