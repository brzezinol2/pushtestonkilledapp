﻿
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Push;
using Xamarin.Forms;

namespace PushTestOnKilledApp
{
    public partial class App : Application
	{
        static App()
        {
            Push.PushNotificationReceived += Push_PushNotificationReceived;
        }

		public App ()
		{
			InitializeComponent();

			MainPage = new PushTestOnKilledApp.MainPage();
		}

		protected override void OnStart ()
		{
            // Handle when your app starts
            AppCenter.LogLevel = LogLevel.Verbose;
            AppCenter.Start("android=600072ae-645a-4dd1-a1af-3173502b4b8b;",
                   typeof(Analytics),
                   typeof(Crashes),
                   typeof(Push));

        }

        static void Push_PushNotificationReceived(object sender, PushNotificationReceivedEventArgs e)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
            {
                System.Diagnostics.Debug.WriteLine("PushTestOnKilledApp.App.PushNotificationReceived.");
                await Current.MainPage.DisplayActionSheet("PushNotificationReceived", null, null, "Ok");
            });
        }

        protected override void OnSleep ()
		{
            // Handle when your app sleeps
        }

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
